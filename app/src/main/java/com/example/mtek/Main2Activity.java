package com.example.mtek;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import static com.example.mtek.R.id.emergency;
import static com.example.mtek.R.id.home;
import static com.example.mtek.R.id.profile;
import static com.example.mtek.R.id.search_button;

public class Main2Activity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        BottomNavigationView navview = findViewById(R.id.bottom);
        navview.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.frame,new frag()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
             Fragment frag  = null;
                switch (menuItem.getItemId())

    {
        case emergency:
            frag = new emergency_frag();
            break;
        case home:
           frag = new Home_frag();
            break;
        case profile:
           frag = new profile_frag();
            break;
        case search_button:
            frag = new settings_frag();
            break;
    }


    getSupportFragmentManager().beginTransaction().replace(R.id.frame,frag).commit();

    return true;

}